<?php

namespace ez\widgets;

use yii\web\AssetBundle;

/**
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @since 1.0
 */
class SwitchButtonAsset extends AssetBundle
{
    public $sourcePath = '@vendor/thanh-vinh/yii2-ez-widgets/assets/bootstrap-switch';
    public $css = [
        'css/bootstrap-switch.min.css',
    ];
    public $js = [
        'js/bootstrap-switch.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
